$directive('if', function (templateHost) {
    return (host, value, updateEvents) => {
        if (updateEvents)
            return;

        if (!eval(value)) {
            host.innerHTML = '';
        }
    }
});

$directive('show', function (templateHost) {
    return (host, value, updateEvents) => {
        if (updateEvents)
            return;

        if (!eval(value)) {
            host.style.display = 'none';
        }
    }
});

$directive('className', function (templateHost) {
    return (host, value, updateEvents) => {
        if (updateEvents)
            return;

        let classNames;
        if (classNames = eval(value)) {
            host.className = classNames;
        }
    }
});

$directive('image', function (templateHost) {
    return (host, value, updateEvents) => {
        if (!updateEvents)
            return;

        let src;
        if (src = eval(value)) {
            if (host.src !== src) {
                host.src = src;
                setTimeout(() => host.src = src, 0);
            }
        }
    }
});

$directive('grid', function (templateHost) {
    return (host, value, updateEvents) => {
        if (updateEvents)
            return;

        let cells;
        if (cells = eval(value)) {
            cells = _.deserialize(cells);
            for (let i = 0; i < cells.x; ++i) {
                let row = document.createElement('tr');
                for (let j = 0; j < cells.y; ++j) {
                    let col = document.createElement('td');
                    col.setAttribute('x', j);
                    col.setAttribute('y', i);
                    row.appendChild(col);
                }
                host.appendChild(row);
            }
        }
    }
});

$directive('grid-highlighter', function (templateHost) {
    return (host, value, updateEvents) => {
        if (updateEvents)
            return;

        let config;
        if (config = eval(value)) {
            config = _.deserialize(config).config;
            for (let settings of config) {
                let baseClassName = settings.baseClassName || '';
                let className = settings.className;
                let x = settings.x;
                let y = settings.y;

                let currentHost = host.querySelector(`td[x="${x}"][y="${y}"]`);
                if (currentHost) {
                    currentHost.className = baseClassName + ' ' + className;
                }
            }
        }
    }
});

$directive('lclick', function (templateHost, scope) {
    return (host, value, updateEvents) => {
        if (!updateEvents)
            return;

        if (value) {
            let handler = scope[value.slice(1, -1)];
            host.onclick = (e) => handler(e) ? $apply() : null;
        }
    }
});

$directive('rclick', function (templateHost, scope) {
    return (host, value, updateEvents) => {
        if (!updateEvents)
            return;

        if (value) {
            let handler = scope[value.slice(1, -1)];
            host.oncontextmenu = (e) => handler(e) ? $apply() : null;
        }
    }
});

$directive('over', function (templateHost, scope) {
    return (host, value, updateEvents) => {
        if (!updateEvents)
            return;

        if (value) {
            let handler = scope[value.slice(1, -1)];
            host.onmouseover = (e) => handler(e) ? $apply() : null;
        }
    }
});

$directive('leave', function (templateHost, scope) {
    return (host, value, updateEvents) => {
        if (!updateEvents)
            return;

        if (value) {
            let handler = scope[value.slice(1, -1)];
            host.onmouseout = (e) => handler(e) ? $apply() : null;
        }
    }
});