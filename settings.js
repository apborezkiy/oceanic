/*
 * Виды состояний игрового процесса
 */
$factory('config', [], function () {
    return {
        board: {
            x_size: 5,
            y_size: 5
        },
        caravels: [
            { size: 3, name: "Maria Doria" },
            { size: 2, name: "Black Pearl" },
            { size: 1, name: "Fly Hollandian" }
        ],
        user: {
            name: "Вы",
            sys_name: "user"
        },
        computer: {
            name: "Мужик",
            sys_name: "computer"
        },
        EModes: {
            titles: {
                idle: "Боевое построение",
                battle_user: "Бой - ваш ход",
                battle_man: "Бой - ход мужика",
                win_user: "Игра окончена",
                win_man: "Игра окончена"
            },
            subtitles: {
                idle: "Расставьте каравеллы",
                battle_user: "Стреляйте по кораблям мужика",
                battle_man: "Мужик атакует ваши каравеллы",
                win_user: "Вы победитель !!!",
                win_man: "Вы проиграли..."
            }
        },
        EMoodes: {
            none: "none",
            idle: "idle",
            angry: "angry",
            happy: "happy",
            game_over: "game_over",
            win: "win"
        },
        dialogs: {
            max: 1,
            none: {
                0: "Стреляй! <br /> Будь уже мужиком!"
            },
            idle: {
                0: "Переворачивай каравеллы <br /> левой кнопкой мыши, <br /> будь мужиком!"
            },
            happy: {
                0: "Получил? <br /> ***ять!",
                1: "Не учи меня жить, ***ять!"
            },
            angry: {
                0: "Прекрати палить <br /> по моим каравеллам, <br /> ***ять!",
                1: "Подпалили, <br /> ***ять!"
            },
            game_over: {
                0: "Тебе просто <br /> повезло, <br /> ***ять!",
                1: "Я все равно <br /> тебя сделаю, <br /> ***ять!"
            },
            win: {
                0: "Ты <br /> проиграл, <br /> ***ять!",
                1: "Ты <br /> неудачник, <br /> ***ять!"
            }
        },
        images: {
            none: "idle",
            idle: "idle",
            happy: "angry",
            angry: "angry",
            win: "angry",
            game_over: "angry"
        }
    }
});

