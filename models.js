/*
 * Виды состояний игрового процесса
 */

// до различия кавычек в ПР просьба не доходить

$factory('EModes', [], function () {
    return {
        idle: "idle",
        battle_user: "battle_user",
        battle_man: "battle_man",
        win_user: "win_user",
        win_man: "win_man"
    }
});

/*
 * Настроение игрока
 */
$factory('EMoodes', [], function () {
    return {
        none: "none",
        idle: "idle",
        angry: "angry",
        happy: "happy",
        game_over: "game_over",
        win: "win"
    }
});

/*
 * Состояние игрока
 */
$factory('EStates', [], function () {
    return {
        search: "search",
        battle: "battle",
        battle_line: "battle_line"
    }
});

/*
 * Состояние игры
 */
$factory('Game',
  ['config', 'Board', 'Player', 'Computer', 'EModes', 'EMoodes'],
  function (config, Board, Player, Computer, EModes, EMoodes) {
    class Game {
        constructor () {
          this.board_player = new Board();
          this.board_computer = new Board();
          this.user = new Player(config.user);
          this.computer = new Computer(config.computer);
          this.mode = EModes.idle;
          this.activeBoardClass = "'board--active'";
        }

        getActiveBoard () {
            if (this.mode == EModes.idle)
                return this.board_player;
            else if (this.mode == EModes.battle_user)
                return this.board_computer;
            else
                return null;
        }

        locateCaravel (x, y, reversed) {
            if (this.board_player.locateCaravel(x, y, reversed)) {
                this.computer.fillBoard(this.board_computer);
                this.mode = EModes.battle_user;
            }
        }

        highlightLocation (x, y, reversed) {
            this.board_player.highlightCaravel(x, y, reversed);
        }

        unhighlightLocation () {
            this.board_player.unhighlightCaravel();
        }

        fire (x, y) {
            if (this.board_computer.fire(x, y)) {
                ++this.user.score;
                this.computer.setMood(EMoodes.angry);
            }

            this.mode = EModes.battle_man;

            $timeout(() => {
                if (!this.checkWinner()) {
                    if (this.computer.fire(this.board_player)) {
                        ++this.computer.score;
                        this.computer.setMood(EMoodes.happy);
                    }
                    this.mode = EModes.battle_user;
                    this.checkWinner();
                }
            }, 3000);
        }

        checkWinner () {
            if (!this.board_computer.isAlive()) {
                this.mode = EModes.win_user;
                this.computer.setFinishMood(EMoodes.game_over);
                return true;
            } else if (!this.board_player.isAlive()) {
                this.mode = EModes.win_man;
                this.computer.setFinishMood(EMoodes.win);
                return true;
            }
        }
    }

    return Game;
});

/*
 * Состояние доски
 */
$factory('Board', ['config', 'Caravela'], function (config, Caravela) {
    class Board {
        constructor () {
            this.x_size = config.board.x_size;
            this.y_size = config.board.y_size;
            this.serializeSizes = this.serializeSizes.bind(this);
            this.caravels = config.caravels.map(e => new Caravela(e.size, e.name));
            this.currentIndex = 0;
            this.currentCaravel = this.caravels[0];
            this.blunders = [];
        }

        serializeSizes () {
            return _.serialize({
                x: this.x_size,
                y: this.y_size
            });
        }

        getHighlighting () {
            let highlighting = [];
            let alives = this.caravels.filter(caravela => caravela.alive);
            for (let alive of alives) {
                let highlight = alive.getPositions();
                highlighting = highlighting.concat(highlight);
            }
            if (this.currentCaravel) {
                let highlight = this.currentCaravel.getPositions();
                if (highlight) {
                    highlight.forEach(e => e.className = 'prelocated');
                    highlighting = highlighting.concat(highlight);
                }
            }
            highlighting = highlighting.concat(this.blunders);
            return _.serialize({config: highlighting});
        }

        highlightCaravel (x, y, reversed) {
            this.currentCaravel.prelocate(x, y, reversed);
        }

        unhighlightCaravel () {
            this.currentCaravel.unlocate();
        }

        locateCaravel (x, y, reversed) {
            let min_distance = Math.sqrt(2.0);
            let isPositionFree = (x, y) => {
                if ((x >= 0 && x < this.x_size) && (y >= 0 && y < this.y_size)) {
                    let alives = this.caravels.filter(caravela => caravela.alive);
                    for (let alive of alives) {
                        let positions = alive.getPositions();
                        for (let position of positions) {
                            if (position.x == x && position.y == y)
                                return false;
                            if (Math.sqrt(Math.pow(position.x - x, 2) + Math.pow(position.y - y, 2.0)) <= min_distance)
                                return false;
                        }
                    }

                    return true;
                } else {
                    return false;
                }
            }

            let located = this.currentCaravel.locate(x, y, reversed, isPositionFree);
            if (located) {
                if (++this.currentIndex < this.caravels.length) {
                    this.currentCaravel = this.caravels[this.currentIndex];
                } else {
                    this.currentCaravel = null;
                    return true;
                }
            }

            return false;
        }

        fire (x, y) {
            let caravel = this.findCaravel(x, y);
            if (caravel) {
                let fired = caravel.fire(x, y);
                return fired;
            } else {
                this.blunders.push({ x: +x, y: +y, className: 'blunder' });
            }
            return false;
        }

        findCaravel (x, y, current) {
            let alives = this.caravels.filter(caravela => caravela.alive);
            for (let alive of alives) {
                let positions = alive.getPositions();
                for (let position of positions) {
                    if (position.x == x && position.y == y) {
                        if (current == alive)
                            return false;

                        return alive;
                    }
                }
            }

            return false;
        }

        isAlive () {
            return this.caravels.filter(caravela => caravela.alive && caravela.fired.length < caravela.size).length;
        }
    }

    return Board;
});

/*
 * Игрок
 */
$factory('Player', ['config'], function (config) {
    class Player {
        constructor (data) {
            this.name = data.name;
            this.sys_name = data.sys_name;
            this.score = 0;
            this.caravells = [];
        }
    }

    return Player;
});

/*
 * Игрок c ИИ
 */
$factory('Computer', ['config', 'Player', 'EMoodes', 'EStates'], function (config, Player, EMoodes, EStates) {
    class Computer extends Player {
        constructor (data) {
            super(data);
            this.mood = EMoodes.none;
            this.dialog = 0;
            this.state = EStates.search;
            this.isVerticalCaravel = null;
            this.firedIndex = null;
            this.fired = [];
            this.step = 1;
            this.caravel = null;

            this.init();
            this.getDialog.bind(this);
        }

        resetMood () {
            if (this.mood != EMoodes.none) {
                do {
                    this.dialog = Math.floor(Math.random() * (config.dialogs.max + 1));
                } while (this.dialog < 0 || this.dialog >= config.dialogs.max);
                this.mood = EMoodes.none;
            }
        }

        init () {
            // начальное настроение - подсказка
            $timeout(() => {
                this.mood = EMoodes.idle;

                $timeout(() => this.resetMood(), 4000);
            }, 1000);
        }

        fillBoard (board) {
            do {
                let min = 0;
                let x = Math.floor(Math.random() * (board.x_size - min)) + min;
                let y = Math.floor(Math.random() * (board.y_size - min)) + min;
                let reserved = Math.floor(Math.random() * (2 - min + 1)) + min;
                board.locateCaravel(x, y, reserved);
            } while (board.currentCaravel);
        }

        getDialog () {
            return config.dialogs[this.mood][this.dialog] || '';
        }

        getImage () {
            return "'" + './assets/' + this.sys_name + '_' + config.images[this.mood] + '.gif' + "'";
        }

        fire (board) {
            // !!! SORRY SORRY SORRY !!!
            // !!! писал на скорую руку лишь бы работало, итак уже очень устал от diff алгоритма
            // сорри за копипаст

            if (this.state == EStates.search) {
                let x;
                let y;
                do {
                    let min = 0;
                    x = Math.floor(Math.random() * (board.x_size - min)) + min;
                    y = Math.floor(Math.random() * (board.y_size - min)) + min;
                } while (~this.fired.findIndex(o => o.x == x && o.y == y) || (x >= +board.x_size) || (y >= +board.y_size) || (x < 0) || (y < 0));

                let fired = board.fire(x, y);
                this.fired.push({ x, y });

                if (fired) {
                    this.firedIndex = this.fired.length - 1;
                    this.state = EStates.battle;
                    let caravel = board.findCaravel(x, y);
                    this.caravel = caravel;
                    if (!caravel.getLives()) {
                        this.state = EStates.search;
                        this.firedIndex = null;
                        this.isVerticalCaravel = null;
                        this.step = 1;
                    }

                    return true;
                }
            } else if (this.state == EStates.battle) {
                let x, y;

                // сначала стреляем вправо
                x = this.fired[this.firedIndex].x + this.step;
                y = this.fired[this.firedIndex].y;

                let forbidden = false;
                if (board.findCaravel(x + 1, y, this.caravel) || ~this.fired.findIndex(o => o.x == x && o.y == y) || (x >= +board.x_size) || (y >= +board.y_size) || (x < 0) || (y < 0))
                    forbidden = true;

                // если нельзя, то стреляем влево
                if (x == board.x_size || forbidden) {
                    x = this.fired[this.firedIndex].x - this.step;
                    y = this.fired[this.firedIndex].y;

                    forbidden = false;
                    if (board.findCaravel(x - 1, y, this.caravel) || ~this.fired.findIndex(o => o.x == x && o.y == y) || (x >= +board.x_size) || (y >= +board.y_size) || (x < 0) || (y < 0))
                        forbidden = true;

                    if (forbidden) {
                        // если нельзя, то остается по вертикали
                        this.isVerticalCaravel = true;
                        this.state = EStates.battle_line;
                        return this.fire(board);
                    }
                }

                // бой
                let fired = board.fire(x, y);
                this.fired.push({ x, y });

                if (fired) {
                    let caravel = board.findCaravel(x, y);
                    if (caravel.getLives()) {

                    } else {
                        this.state = EStates.search;
                        this.isVerticalCaravel = null;
                        this.step = 1;
                    }
                }

                if (this.isVerticalCaravel == null && this.fired.length > 1) {
                    this.state = EStates.battle_line;
                    this.isVerticalCaravel = false;
                }

                if (fired)
                    return true;
            } else if (this.state === EStates.battle_line) {
                let x, y;

                if (this.isVerticalCaravel) {
                    // сначала стреляем вниз
                    x = this.fired[this.firedIndex].x;
                    y = this.fired[this.firedIndex].y + this.step;

                    let forbidden = false;
                    if (board.findCaravel(x, y + 1, this.caravel) || ~this.fired.findIndex(o => o.x == x && o.y == y) || (x >= +board.x_size) || (y >= +board.y_size) || (x < 0) || (y < 0))
                        forbidden = true;

                    // если нельзя, то стреляем наверх
                    if (x == board.x_size || forbidden) {
                        x = this.fired[this.firedIndex].x;
                        y = this.fired[this.firedIndex].y - this.step;

                        forbidden = false;
                        if (board.findCaravel(x, y - 1, this.caravel) || ~this.fired.findIndex(o => o.x == x && o.y == y) || (x >= +board.x_size) || (y >= +board.y_size) || (x < 0) || (y < 0))
                            forbidden = true;

                        // если нельзя, то остается увеличиваем шаг
                        if (forbidden) {
                            ++this.step;
                            if (this.step > board.x_size + board.y_size)
                                throw `# UNREACHABLE 1`;

                            return this.fire(board);
                        }
                    }
                } else {
                    // сначала стреляем вправо
                    x = this.fired[this.firedIndex].x + this.step;
                    y = this.fired[this.firedIndex].y;

                    let forbidden = false;
                    if (board.findCaravel(x + 1, y, this.caravel) || ~this.fired.findIndex(o => o.x == x && o.y == y) || (x >= +board.x_size) || (y >= +board.y_size) || (x < 0) || (y < 0))
                        forbidden = true;

                    // если нельзя, то стреляем влево
                    if (x == board.x_size || forbidden) {
                        x = this.fired[this.firedIndex].x - this.step;
                        y = this.fired[this.firedIndex].y;

                        forbidden = false;
                        if (board.findCaravel(x - 1, y, this.caravel) || ~this.fired.findIndex(o => o.x == x && o.y == y) || (x >= +board.x_size) || (y >= +board.y_size) || (x < 0) || (y < 0))
                            forbidden = true;

                        // если нельзя, то остается увеличиваем шаг
                        if (forbidden) {
                            if (this.caravel.getSize() <= this.step * 2 + 1) {
                                this.isVerticalCaravel = true;
                            } else {
                                ++this.step;

                                if (this.step < board.x_size + board.y_size)
                                    throw `# UNREACHABLE 2`;
                            }
                            return this.fire(board);
                        }
                    }
                }

                // бой
                let fired = board.fire(x, y);
                this.fired.push({ x, y });

                if (fired) {
                    let caravel = board.findCaravel(x, y);
                    if (caravel.getLives()) {

                    } else {
                        this.state = EStates.search;
                        this.isVerticalCaravel = null;
                        this.step = 1;
                    }

                    return true;
                }
            } else {
                throw `# UNREACHABLE`;
            }
        }

        setMood (mood) {
            this.mood = mood;
            $timeout(() => this.resetMood(), 2000);
        }

        setFinishMood (mood) {
            this.mood = mood;
        }
    }

    return Computer;
});

/*
 * Корабль
 */
$factory('Caravela', [], function () {
    class Caravela {
        constructor (size, name) {
            this.size = size;
            this.name = name;
            this.x = null;
            this.y = null;
            this.fired = [];
        }

        locate (x, y, reversed, isPositionFree) {
            this.x = +x;
            this.y = +y;
            this.reversed = reversed;

            let positions = this.getPositions ();
            let isFree = !positions.filter(e => !isPositionFree(e.x, e.y)).length;
            if (isFree) {
                this.alive = true;
            }

            return isFree;
        }

        prelocate (x, y, reversed) {
            this.x = +x;
            this.y = +y;
            this.reversed = reversed;
        }

        unlocate () {
            this.x = null;
            this.y = null;
        }

        fire (x, y) {
            let firedIndex = this.fired.findIndex(e => (e.x == +x) && (e.y == +y));
            if (!(~firedIndex)) {
                this.fired.push({ x: +x, y: +y });
                return true;
            }

            return false;
        }

        getPositions () {
            if (this.x == null || this.y == null)
                return null;

            if (this.reversed) {
                let positions = [];
                for (let i = 0; i < this.size; ++i) {
                    let isFired = ~this.fired.findIndex(e => (e.x == this.x + i) && (e.y == this.y));
                    //console.log(this.fired, isFired, this.x + i, this.y)
                    positions.push({ x: this.x + i, y: this.y, className: isFired ? 'fired' : 'located' });
                }
                return positions;
            } else {
                let positions = [];
                for (let i = 0; i < this.size; ++i) {
                    let isFired = ~this.fired.findIndex(e => (e.x == this.x) && (e.y == this.y + i));
                    //console.log(this.fired, isFired)
                    positions.push({ x: this.x, y: this.y + i, className: isFired ? 'fired' : 'located' });
                }
                return positions;
            }
        }

        getLives () {
            return this.size - this.fired.length;
        }

        getSize () {
            return this.size;
        }
    }

    return Caravela;
});

