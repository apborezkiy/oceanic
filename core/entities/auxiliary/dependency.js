/*
 * Дескриптор зависимости
 */
class Dependency {
    constructor (name, injector) {
        this.name = name;
        this.injector = injector;
    }

    /*
     * Разрешить зависимость
     */
    get () {
        return this.injector.get(this.name);
    }

    /*
     * Создание дескрипторов зависимостей
     */
    static create (names, injector) {
        let dependencies = [];

        for (let name of names) {
            dependencies.push(new Dependency(name, injector));
        }

        return dependencies;
    }
}
