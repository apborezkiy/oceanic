/*
 * Пользовательская сущность
 */
class Entity {
    constructor (name, type, dependencies, provider) {
        this.name = name;
        this.type = type;
        this.dependencies = dependencies;
        this.provider = provider;
    }

    hasType (type) {
        return this.type === type;
    }

    getName () {
        return this.name;
    }

    provide (idAllocator) {
        let instances = this.dependencies.map(dependency => dependency.get());

        return this.provider(instances, idAllocator);
    }
}
