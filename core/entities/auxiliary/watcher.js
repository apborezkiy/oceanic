'use strict';

/*
 * Наблюдатель
 */
class Watcher {
    constructor (callback) {
        this.callback = callback;
        this.cached = undefined;
    }

    /*
     * Грязная проверка
     */
    run () {
        var result = this.callback();
        
        if (_.isObject(result) && _.isObject(this.cached)) {
            if (!_.isEqual(result, this.cached)) {
                this.cached = _.copy(result);
                return true;
            }
        } else {
            if (result !== this.cached) {
                this.cached = _.copy(result);
                return true;
            }
        }

        return false;
    }

    /*
     * Текущее значение
     */
    getValue () {
        return this.cached;
    }
}
