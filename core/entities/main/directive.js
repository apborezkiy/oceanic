'use strict';

/*
 * Директива
 *
 * Используется как аттрибут
 */
class Directive {
    constructor (name, callback, dom, render, instances, args) {
        this.name = name;
        this.callback = callback;
        this.instances = instances;
        this.dom = dom;
        this.render = render;

        this.controller = args.controller;
        this.host = args.host;
        this.id = args.id;

        // наблюдатель за выражением аттрибута
        this.watcher = null;

        // усилитель шаблона
        this.amplifier = null;

        this.init();
    }

    /*
     * Инициализация директивы
     */
    init () {
        console.log (`# provideDirective create: ${this.name} \n`);
        //console.log (`  host: `, this.host);

        // установка идентификатора директивы
        this.dom.setDirectiveId(this.host, this.id);

        // усилитель шаблона
        this.controller.applyTemplateMiddleware((...args) => this.templateMiddleware(...args));

        // получить выражение директивы
        let expressionTemplate = this.dom.getDirectiveValue(this.host, this.name);

        // получить область видимости
        let scope = this.controller.getScope();

        // вычислить выражение
        let getExpression = () => this.render.render(expressionTemplate, scope);

        // создать наблюдатель за выражением
        this.watcher = new Watcher(() => eval(getExpression()));

        // запуск директивы
        this.amplifier = this.callback.call(this, this.host, scope) || ((html) => html);
    }

    /*
     * Проверка наблюдателя аттрибута
     */
    digest (force) {
        let needDigest = false;

        if (this.watcher.run() || force) {
            needDigest = true;
        }

        return needDigest;
    }

    /*
     * Усилитель шаблона
     */
    templateMiddleware (html, updateEvents) {
        //console.log(`# amplifying cause of ${this.name}`, html, this.id);
        let host = this.dom.getDirectiveById(html, this.id);
        if (!host)
            throw `# LOST DIRECTIVE ${this.name} WITH ID ${this.id}!`;

        this.amplifier(host, this.dom.getDirectiveValue(host, this.name), updateEvents);

        return html;
    }

    /*
     * Родительский контроллер
     */
    getController () {
        return this.controller;
    }

    /*
     * Геттеры
     */

    getType () {
        return 'd';
    }
}
