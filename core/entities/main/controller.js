'use strict';

/*
 * Контроллер
 * 1. Имеет свой шаблон
 * 2. Имеет область видимости
 * 3. Поддерживает внедрение фабрик
 *
 * Пример использования:
 *   <div controller="name"></div>
 */
class Controller {
    constructor (name, callback, dom, render, instances, idAllocator, args) {
        this.name = name;
        this.callback = callback;
        this.instances = instances;
        this.dom = dom;
        this.render = render;
        this.idAllocator = idAllocator;

        this.host = args.host;
        this.directiveNames = args.directiveNames;

        this.template = null;
        this.scope = {};
        this.watcher = new Watcher(() => this.scope);

        this.middlewares = []; // усилители шаблона

        this.init();
    }

    /*
     * Инициализация контроллера
     */
    init () {
        console.log (`# provideController create: ${this.name} \n`);

        // расставление идентификаторов директив
        for (let directiveName of this.directiveNames) {
            let hosts = this.dom.getDirectiveHosts(directiveName);
            for (let i = 0; i < hosts.length; ++i) {
                this.dom.setDirectiveId(hosts[i], this.idAllocator());
            }
        }

        this.template = this.dom.getTemplate(this.host);

        // инициализация области видимости
        var variables = this.template.match(/{{[a-z1-9]*}}/i);
        if (variables) {
            variables.forEach((variable) => {
                var name = variable.replace(/{{([a-z1-9]*)}}/i, '$1');
                this.scope[name] = null;
            });
        }

        // запуск контроллера
        this.callback.apply(this.scope, this.instances.map(instance => instance.getProduct()));

        console.log ("  with scope: ", this.scope);
    }

    /*
     * Удаление контроллера
     */
    destroy () {
        console.log (`# provideController delete: ${this.name} \n`);
    }

    /*
     * Обновление шаблона
     */
    update () {
        let content = this.render.render(this.template, this.scope);
        //console.log (`# update provideController: ${this.name} \n`);
        let element = this.dom.createElement(content);
        //console.log ("  template: " + this.template);
        for (let middleware of this.middlewares) {
            element = middleware(element);
        }

        // diff алгоритм
        // на него пришлось потратить кучу времени
        function diff (newer, older, level = 0) {
            if (newer.hasChildNodes() && older.hasChildNodes()) {
                let newers = newer.childNodes;
                let olders = older.childNodes;
                if (newers.length !== olders.length) {
                    return this.dom.replace(older, newer);
                } else {
                    for (let l = 0; l < newers.length; ++l) {
                        older = diff.call(this, newers[l], olders[l], level + 1);
                    }
                    return older;
                }
            } else {
                return this.dom.replace(older, newer);
            }
        }

        diff.call(this, element, this.host);

        for (let middleware of this.middlewares) {
            this.host = middleware(this.host, true);
        }
    }

    /*
     * Проверка всех наблюдателей
     */
    digest (force) {
        if (this.watcher.run() || force) {
            this.update();
            return true;
        }

        return false;
    }

    /*
     * Модификаторы шаблона
     */
    applyTemplateMiddleware (callback) {
        this.middlewares.push(callback);
    }

    /*
     * Геттеры
     */

    getScope () {
        return this.scope;
    }

    getHost () {
        return this.host;
    }

    getName () {
        return this.name;
    }

    getType () {
        return 'c';
    }

    getDependencies () {
        return this.instances;
    }
}