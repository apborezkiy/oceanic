'use strict';

/*
 * Фабрика
 *
 * Внедряемый объект.
 * Реализует бизнес функционал.
 */
class Factory {
    constructor (name, callback, instances, args) {
        this.name = name;
        this.callback = callback;
        this.instances = instances;

        this.product = this.init();
        this.watcher = new Watcher(() => this.product);
    }

    /*
     * Инициализация
     */
    init () {
        console.log (`# Factory create: ${this.name} \n`);

        return this.callback.apply(this, this.instances.map(instance => instance.getProduct()));
    }

    /*
     * Продукт фабрики
     */
    getProduct () {
        return this.product;
    }

    /*
     * Проверка всех наблюдателей
     */
    digest (force) {
        if (this.watcher.run() || force) {
            return true;
        }

        return false;
    }

    /*
     * Геттеры
     */

    getType () {
        return 'f';
    }
}