'use strict';

/*
 * Конфиг фреймворка
 */
var config = {
    max_digest: 3
};

var framework = new Framework(config);
window.$ = framework;

window.$controller = $.registerController.bind($);
window.$directive = $.registerDirective.bind($);
window.$factory = $.registerFactory.bind($);

window.$digest = $.digest.bind($);
window.$apply = $.apply.bind($);
window.$timeout = $.timeout.bind($);