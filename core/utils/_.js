/*
 * Проверки типов
 */
function isObject (o) {
    return typeof o === "object";
}

function isFunction (o) {
    return typeof o === "function";
}

/*
 * Поверхностное сравнение
 */
function isEqual (o1, o2) {
    var o1ks = Object.keys(o1);
    var o2ks = Object.keys(o2);

    for (var o1k of o1ks) {
        if (!~o2ks.indexOf(o1k)) {
            return false;
        }
    }

    for (var o2k of o2ks) {
        if (!~o1ks.indexOf(o2k)) {
            return false;
        }
    }

    for (var ok of o1ks) {
        if (o1[ok] !== o2[ok]) {
            return false;
        }
    }

    return true;
}

/*
 * Копирование объекта
 */
function copy (o) {
    if (isObject(o))
        return Object.assign({}, o);

    return o;
}

/*
 * Ключи объекта
 */
function keys (o) {
    return Object.keys(o);
}

/*
 * Зашифровать кавычки
 */
function sanitize (text) {
    return "'" + text.replace(/["]/g, '#') + "'";
}

/*
 * Расшифровать кавычки
 */
function desanitize (text) {
    return text.replace(/[#]/g, '"');
}

/*
 * Сериализация объекта
 */
function serialize (o) {
    return sanitize(JSON.stringify(o));
}

/*
 * Десериализация объекта
 */
function deserialize (o) {
    return JSON.parse(_.desanitize(o));
}

/*
 * Экспорт
 */
window._ = {
    isObject,
    isFunction,
    isEqual,
    copy,
    keys,
    sanitize,
    desanitize,
    serialize,
    deserialize
}