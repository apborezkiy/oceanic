'use strict';

/*
 * Микро ангуляр подобный фреймворк который я написал за 10 часов :)))
 * Правда еще целый день ушел на разработку diff алгоритма DOM дерева.
 * Чтобы работала анимация нужно более минималистично накладывать патчи (на 1 элемент в глубину больше), но времени итак уже потратил уйму на него.
 *
 * Что в нем есть?
 * 1. Контроллеры (имеют область видимости и шаблон)
 * 2. Директивы (имеют доступ к DOM элементу-хосту на котором они объявлены и области видимости контроллера)
 * 3. Фабрики как внедряемые объекты
 * 4. Шаблонизатор с однонаправленным data binding
 */

/*
 * Фасад фреймворка.
 */
class Framework {
    constructor(config) {
        this.config = config;

        // композиция фасада
        this.render = new Render();
        this.manager = new Manager();
        this.injector = new Injector(this.manager);
        this.dom = new Dom();
    }

    /*
     * Регистрация провайдеров пользовательских сущностей
     */
    registerController (name, factories, callback) {
        let dependencies = Dependency.create(factories, this.injector);
        let provider = (factories, idAllocator) =>
          (args) => new Controller(name, callback, this.dom, this.render, factories, idAllocator, args);

        this.manager.registerEntity(name, 'c', dependencies, provider);
    }

    registerDirective (name, callback) {
        let provider = (factories) =>
          (args) => new Directive(name, callback, this.dom, this.render, factories, args);

        this.manager.registerEntity(name, 'd', [], provider);
    }

    registerFactory (name, factories, callback) {
        let dependencies = Dependency.create(factories, this.injector);
        let provider = (factories) =>
          (args) =>
            new Factory(name, callback, factories, args);

        this.manager.registerEntity(name, 'f', dependencies, provider);
    }

    /*
     * Начальная загрузка приложения.
     *
     * 1. Создание экземпляров присутствующих в дереве контроллеров.
     * 2. Создание экземпляров присутствующих в дереве директив.
     * 3. Digest цикл.
     */
    boot () {
        // контроллеры
        {
            let hosts = this.dom.getControllersHosts();
            for (let i = 0; i < hosts.length; ++i) {
                let host = hosts[i];
                let name = this.dom.getControllerName(host);
                this.createController(name, host);
            }
        }
        // директивы
        {
            let entities = this.manager.getEntities('d');
            for (let entity of entities) {
                let name = entity.getName();
                let hosts = this.dom.getDirectiveHosts(name);
                for (let i = 0; i < hosts.length; ++i) {
                    let host = hosts[i];
                    this.createDirective(name, host);
                }
            }
        }

        this.digest();
    }

    /*
     * Digest цикл
     */
    digest (iteration = 0, force = false) {
        console.time(force ? "digest forced" : "digest");

        let needDigest = false;

        this.manager.getEntitiesInstances('c').forEach(controller => {
            let forceController = false;

            // зависимости контроллера
            controller.getDependencies().forEach(dependency => {
                if (dependency.digest()) {
                    forceController = true;
                }
            });

            // контроллер
            if (controller.digest(force || forceController)) {
                needDigest = true;

                // директивы, принадлежащие контроллеру
                this.manager.getEntitiesInstances('d').forEach(directive => {
                    if (directive.getController().getName() === controller.getName())
                        directive.digest(force);
                });
            }
        });

        if (needDigest) {
            if (iteration > config.max_digest) {
                throw "# DIGEST DEPTH EXCEEDED !";
            }

            this.digest(iteration + 1);
        }

        console.timeEnd(force ? "digest forced" : "digest");
    }

    /*
     * Digest цикл без проверки наблюдателей
     *
     * TODO: Используется по причине отсутствия deep object compare, так как влом его было писать
     */
    apply () {
        this.digest(0, true);
    }

    /*
     * Отложенное выполнение в контексте области видимости
     */
    timeout (callback, timeout) {
        setTimeout(() => {
            callback();
            this.apply();
        }, timeout);
    }

    /*
     * Cоздание экземпляра контроллера
     */
    createController(name, host) {
        let directiveNames = [];

        for (let directive of this.manager.getEntities('d')) {
            if (this.dom.getDirectiveHosts(directive.getName())) {
                directiveNames.push(directive.getName());
            }
        }

        let args = { host, directiveNames };
        this.manager.instantiateEntity(name, args);
    }

    /*
     * Cоздание экземпляра директивы
     */
    createDirective (name, host) {
        // находим родительский контроллер
        let node = host;
        let controllerName;

        do {
            controllerName = this.dom.getControllerName(node);
        } while (!controllerName && (node = node.parentNode));

        if (!controllerName) {
            throw `# ORPHAN DIRECTIVE: ${name} !`;
        }

        let controller = this.manager.getEntityInstance('c', (instance) =>
            instance.getName() === controllerName && instance.getHost() === node);

        if (!controller) {
            throw `# ORPHAN DIRECTIVE: ${name} !`;
        }

        let id = this.dom.getDirectiveValue(host, 'did');

        let args = { host, id, controller };
        this.manager.instantiateEntity(name, args);
    }
}
