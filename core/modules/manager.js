/*
 * Менеджер основных сущностей
 */
class Manager {
    constructor () {
        this.entities = {};
        this.instances = {}; // набор активных сущностей
        this.ordinal = 0; // порядковый номер управляемой сущности
    }

    /*
     * @param {string} name Имя сущности.
     * @param {Dependency[]} dependencies Дескрипторы зависимостей.
     * @param {Function} provider Провайдер сущности.
     */
    registerEntity (name, type, dependencies, provider) {
        this.entities[name] = new Entity(name, type, dependencies, provider);
    }

    instantiateEntity (name, args) {
        if (!this.entities[name]) {
            throw `# NO SUCH ENTITY WITH NAME: ${name}!`
        }

        if (!this.instances[name])
            this.instances[name] = [];

        let instance = this.entities[name].provide(() => ++this.ordinal)(args);
        this.instances[name].push(instance);

        return instance;
    }

    getEntitiesInstances (type) {
        let instances = [];
        for (let name of _.keys(this.instances)) {
            instances = instances.concat(this.instances[name].filter(instance => instance.getType() === type));
        }
        return instances;
    }

    getEntityInstance (type, comparator) {
        let instances = this.getEntitiesInstances(type).filter(comparator);

        if (instances.length !== 1)
            throw `# NO SUITABLE ENTITY FOR TYPE: ${type}!`

        return instances[0];
    }

    getEntities (type) {
        let entities = [];
        for (let name of _.keys(this.entities)) {
            if (this.entities[name].hasType(type));
                entities.push(this.entities[name]);
        }
        return entities;
    }
}
