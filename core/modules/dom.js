/*
 * Слой абстракции над DOM
 */
class Dom {
    getControllersHosts () {
        return document.querySelectorAll("[controller]");
    }

    getControllerName(host) {
        return host.getAttribute("controller");
    }

    getDirectiveHosts (name) {
        return document.querySelectorAll(`[${name}]`);
    }

    getDirectiveById (element, id) {
        return element.querySelector(`[did="${id}"]`);
    }

    setDirectiveId (host, id) {
        return host.setAttribute('did', `${id}`);
    }

    getDirectiveValue(host, name) {
        return host.getAttribute(`${name}`);
    }

    getTemplate(element) {
        return element.innerHTML;
    }

    createElement(content) {
        let element = document.createElement('div');
        element.innerHTML = content;
        return element;
    }

    setContent(element, content) {
        element.innerHTML = '';
        element.appendChild(content);
    }

    replace(older, newer) {
        if (older.nodeType == 1) {
            older.parentNode.innerHTML = newer.parentNode.innerHTML;
        }
        if (older.nodeType == Node.TEXT_NODE && older.nodeValue != newer.nodeValue) {
            older.parentNode.innerHTML = newer.parentNode.innerHTML;
        }

        return older;
    }
}
