/*
 * Шаблонизатор
 */
class Render {
    /*
     * Отрисовка шаблона
     */
    render (template, values) {
        let html = template;
        let variables = Object.keys(values);

        for (let i = 0; i < variables.length; ++ i) {
            let variable = variables[i];
            html = this.renderVariable(html, variable, values[variable]);
        }

        html = this.renderExpressions(html, values);

        return html;
    }

    /*
     * Вспомогательные методы
     */

    renderVariable (template, name, value) {
        return template.replace(new RegExp('{{' + name + '}}', 'g'), '' + value);
    }

    renderExpressions (template, scope) {
        return template.replace(new RegExp('{#(.+?)#}', 'g'), function (str, p) {
            var $$ = scope;
            return eval(p);
        })
    }
}
