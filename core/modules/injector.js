/*
 * Разрешение зависимостей
 */
class Injector {
    constructor (manager) {
        // экземпляры внедряемых сущностей
        this.instances = {}
        this.manager = manager; // провайдер основных сущностей
    }

    /*
     * @param {string} name Имя зависимости.
     */
    get (name) {
        if (!this.instances[name]) {
            this.instances[name] = this.manager.instantiateEntity (name, []);
            console.log(`# name created ${name}`, this.instances[name]);
        }

        return this.instances[name];
    }
}
