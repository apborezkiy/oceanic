$controller('ctrl_application', ['config', 'Game', 'EModes'], function (config, Game, EModes) {

    this.game = new Game(); // Состояние игры
    this.config = config;
    this.reversed = false;
    this.x = null;
    this.y = null;

    /*
     * UI события
     */
    this.handleClickBoard = (e) => {
        let target = e.target;
        let x = target.getAttribute('x');
        let y = target.getAttribute('y');

        if (x != null && y != null) {

            if (this.game.mode == EModes.idle) {
                this.game.locateCaravel(x, y, this.reversed);
            } else if (this.game.mode == EModes.battle_user) {
                this.game.fire(x, y);
            }

            return true;
        }

        return false;
    }

    this.handleBoardOver = (e) => {
        let target = e.target;
        let x = target.getAttribute('x');
        let y = target.getAttribute('y');
        let changed = false;

        if (this.x != x) {
            this.x = x;
            changed = true;
        }
        if (this.y != y) {
            this.y = y;
            changed = true;
        }

        if (x != null && y != null && changed) {
            if (this.game.mode == EModes.idle) {
                this.game.highlightLocation(x, y, this.reversed);

                return true;
            }
        }

        return false;
    }

    this.handleBoardLeave = (e) => {
        let target = e.target;
        let x = target.getAttribute('x');
        let y = target.getAttribute('y');
        let changed = false;

        if (this.x != null) {
            this.x = null;
            changed = true;
        }
        if (this.y != null) {
            this.y = null;
            changed = true;
        }

        if (changed) {
            if (this.game.mode == EModes.idle) {
                this.game.unhighlightLocation();

                return true;
            }
        }

        return false;
    }

    this.handleReverse = (e) => {
        e.preventDefault();
        this.reversed = !this.reversed;

        return true;
    }
});